<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name_ar'=>'nullable|string|max:255',
            'name_en'=>'nullable|string|max:255',
            'category_id'=>'nullable|exists:categories,id',
            'image'=>'nullable|mimes:jpeg,jpg,png,gif|max:10000',
        ];
    }
}
