<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    public static function collection($data)
    {
        /*
         This simply checks if the given data is and instance of Laravel's paginator classes
         and if it is,
        it just modifies the underlying collection and returns the same paginator instance
        */
        if (is_a($data, \Illuminate\Pagination\AbstractPaginator::class)) {
            $data->setCollection(
                $data->getCollection()->map(function ($listing) {
                    return new static($listing);
                })
            );

            return $data;
        }

            return  parent::collection($data);
    }
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'product_id' =>$this->product_id,
            'product_name_ar' =>$this->product->name_ar,
            'product_name_en' =>$this->product->name_en,
            'product_image' =>$this->product->image,
            'user_id' =>$this->user_id,
            'username' =>$this->user->username,
            'quantity' =>$this->quantity,
        ];
    }
}
