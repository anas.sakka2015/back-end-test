<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    public static function collection($data)
    {
        /*
         This simply checks if the given data is and instance of Laravel's paginator classes
         and if it is,
        it just modifies the underlying collection and returns the same paginator instance
        */
        if (is_a($data, \Illuminate\Pagination\AbstractPaginator::class)) {
            $data->setCollection(
                $data->getCollection()->map(function ($listing) {
                    return new static($listing);
                })
            );

            return $data;
        }

            return  parent::collection($data);
    }
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'name_ar' =>$this->name_ar,
            'name_en' =>$this->name_en,
            'image' =>$this->image,
            'category_id' =>$this->category_id,
            'category_name_ar' =>$this->category->name_ar,
            'category_name_en' =>$this->category->name_en,
            'user_orders' =>$this->user,


        ];
    }
}
