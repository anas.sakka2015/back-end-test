<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminLoginRequest;
use App\Http\Resources\AdminResource;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminAuthController extends ApiController
{
   public function  adminLogin(AdminLoginRequest $request){
    $admin = Admin::where('email', $request->email)->first();
        if($admin){
            if (!Hash::check($request->password, $admin->password)) {
                return $this->error(
                    'incorrect password',
                    'The password you entered is incorrect.',
                    403
                );
            }

            $admin->tokens()->where('tokenable_id', '=', $admin->id)->where('tokenable_type','App\Models\Admin')->delete();
            $token = $admin->createToken('my-app-token', ['Admin'])->plainTextToken;
            $response = [
                'success' => true,
                'admin' => new AdminResource($admin),
                'token' => $token,
                'role' => 'Admin'
            ];
            return $this->success($response, 200, 'You are logged in successfully');

        }else{
            return $this->error(["This admin is not found"], "This admin is not found", 200);

        }
   }


}
