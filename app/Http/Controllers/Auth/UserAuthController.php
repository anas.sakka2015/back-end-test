<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserAuthController extends ApiController
{
    public function  userLogin(UserLoginRequest $request){
        $user = User::where('email', $request->email)->first();
            if($user){
                if (!Hash::check($request->password, $user->password)) {
                    return $this->error(
                        'incorrect password',
                        'The password you entered is incorrect.',
                        403
                    );
                }

                $user->tokens()->where('tokenable_id', '=', $user->id)->where('tokenable_type','App\Models\User')->delete();
                $token = $user->createToken('my-app-token', ['User'])->plainTextToken;
                $response = [
                    'success' => true,
                    'user' => new UserResource($user),
                    'token' => $token,
                    'role' => 'User'
                ];
                return $this->success($response, 200, 'You are logged in successfully');

            }else{
                return $this->error(["This user is not found"], "This user is not found", 200);

            }
       }
       public function register(UserRegisterRequest $request)
       {
           $data = $request->validated();
           $data['password'] = Hash::make($request['password']);
           $user = User::create($data);
           $token = $user->createToken('my-app-token', ['User'])->plainTextToken;
           $response = [
               'success' => true,
               'user' => new UserResource($user),
               'token' => $token
           ];
           return $this->success($response, 200, 'You are logged in successfully');
       }
}
