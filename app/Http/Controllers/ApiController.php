<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

/**
 * Class ApiController
 * @package App\Http\Api\V1\Controllers
 */
class ApiController extends Controller
{

    protected $user;
    protected $user_id;
    protected $profile;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = $request->user();
            $this->user_id = $this->user ? $this->user->id : null;
            $this->profile = $this->user;
            return $next($request);
        });
    }

    protected function success($data, int $code)
    {
        return response()->json([
            'data' => $data,
            'code' => $code
        ], $code);
    }

    protected function error($errors = [], string  $message = null, int $code)
    {
        return response()->json([
            'message' => $message ?? "An error encountered while performing the operation",
            'errors' => $errors,
            'code' => $code,
        ], $code);
    }
}
