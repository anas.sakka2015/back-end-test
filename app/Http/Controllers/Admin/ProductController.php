<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Models\Product;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Http\Resources\ProductResource;
use Illuminate\Support\Facades\Storage;

class ProductController extends ApiController
{

    public function index()
    {
        $product = Product::with('category','user')->get();
        return $this->success(ProductResource::collection($product), 200);
    }
    public function store(StoreProductRequest $request)
    {
        $data=$request->validated();
        if ($request->hasFile('image')) {
            $image = $request->file('image')->storePublicly('product', 'public');
            $data['image'] = $image;
         }
        $product = Product::create($data);
        return $this->success("Product was created successfully", 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return $this->success( new ProductResource($product), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return $this->success( "Product was updated successfully", 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateProductRequest  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        $data=$request->validated();
        if($request->hasFile('image')){
                if ( Storage::exists('public/'.$product->image)) {
                    Storage::delete('public/'. $product->image);
                }
                $image = $request->file('image')->storePublicly('product', 'public');
                $data['image'] = $image;
            }else{
                $data['image'] = $product->image;
            }
        $product->update($data);
        return $this->success( "Product was updated successfully", 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        if (Storage::exists('public/'.$product->image)) {
            Storage::delete('public/'. $product->image);
        }
        $product->delete();
        return $this->success( "Product was deleted successfully", 200);
    }
}
