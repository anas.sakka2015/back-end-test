<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderProductRequest;
use App\Http\Resources\CategoryFrontEndResource;
use App\Http\Resources\OrderResource;
use App\Http\Resources\ProductFrontEndResource;
use App\Models\Category;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Spatie\FlareClient\Api;

class FrontEndController extends ApiController
{
   public function orders(){
    $user=auth('sanctum')->user();
    $orders=Order::where('user_id',$user->id)->with('product')->get();
    return $this->success(OrderResource::collection($orders), 200);
   }
   public function orderProduct(OrderProductRequest $request){
    $user=auth('sanctum')->user();
    $data=$request->validated();
    $data['user_id']= $user->id;

    foreach($request->products as $product){

      $order=Order::where('product_id',$product['product_id'])->where('user_id',$user->id)->first();

      if($order){
         $order->update(['quantity'=>$order->quantity+$product['quantity']]);
      }else{
      $data['product_id']= $product['product_id'];
      $data['quantity']= $product['quantity'];
      $order=Order::create($data);
   }

    }
    return $this->success('The order created successfully', 200);
   }
   public function categories(){
     $category= Category::paginate(6);
      return $this->success(CategoryFrontEndResource::collection($category), 200);
   }
   public function products(Category $category){
     $products= Product::where('category_id',$category->id)->paginate(6);
     return $this->success(ProductFrontEndResource::collection($products), 200);
  }
}
