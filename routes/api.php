<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Auth\AdminAuthController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Api\FrontEndController;
use App\Http\Controllers\Auth\UserAuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/// --------------- Admin Apis --------------
Route::post('login_admin',[AdminAuthController::class,'adminLogin'])->name('login');

Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth:sanctum','abilities:Admin']], function () {

    Route::apiResource('categories', CategoryController::class);

    Route::apiResource('products', ProductController::class);

    Route::apiResource('admins', AdminController::class);

    Route::get('user_orders/{user}',[OrderController::class,'userOrder'])->name('orders.user');

});

/// ---------------  User Apis --------------

Route::post('login',[UserAuthController::class,'userLogin'])->name('user.login');
Route::post('register',[UserAuthController::class,'register'])->name('user.register');

Route::group(['middleware' => ['auth:sanctum','abilities:User']], function () {
    Route::get('orders',[FrontEndController::class,'orders'])->name('orders');
    Route::post('order/product',[FrontEndController::class,'orderProduct'])->name('order.product');
    Route::get('categories',[FrontEndController::class,'categories'])->name('categories');
    Route::get('products/{category}',[FrontEndController::class,'products'])->name('products');
});
